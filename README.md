# AgendaCalendarView

## Work In Progress (WIP)

This library replicates the basic features of the Calendar and Agenda views from the Sunrise Calendar (now Outlook) app, coupled with some small design touch from the Google Calendar app.  

![](demo.gif)

Usage
===============================

First, add the JitPack repository to root build.gradle:

```groovy
   allprojects {
         repositories {
              ...
              maven { url 'https://jitpack.io' }
         }
   }
````

Then add the dependency:

```groovy
    compile 'com.gitlab.joielechong:agendacalendarview:1.4.0'
````  

Declare this view in your layout like below, providing your own theme and colors if you want.

```java
    <com.rilixtech.agendacalendarview.AgendaCalendarView
        android:id="@+id/agenda_calendar_view"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        agendaCalendar:acv_agendaCurrentDayTextColor="@color/theme_primary"
        agendaCalendar:acv_calendarColor="@color/theme_primary"
        agendaCalendar:acv_calendarCurrentDayTextColor="@color/calendar_text_current_day"
        agendaCalendar:acv_calendarDayTextColor="@color/theme_text_icons"
        agendaCalendar:acv_calendarHeaderColor="@color/theme_primary_dark"
        agendaCalendar:acv_calendarPastDayTextColor="@color/theme_light_primary"
        agendaCalendar:acv_fabColor="@color/theme_accent" />
````  

Then configure it in your code with a start and end date associated with a list of events:  
```java
        // minimum and maximum date of our calendar
        // 2 month behind, one year ahead, example: March 2015 <-> May 2015 <-> May 2016
        Calendar minDate = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();

        minDate.add(Calendar.MONTH, -2);
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        maxDate.add(Calendar.YEAR, 1);

        List<CalendarEvent> eventList = new ArrayList<>();
        mockList(eventList);

        mAgendaCalendarView.init(eventList, minDate, maxDate, Locale.getDefault(), this);
````  

The event list contains BaseCalendarEvent instances, see the description of the parameters:
```java
    /**
     * Initializes the event
     *
     * @param id          The id of the event.
     * @param color       The color of the event.
     * @param title       The title of the event.
     * @param description The description of the event.
     * @param location    The location of the event.
     * @param dateStart   The start date of the event.
     * @param dateEnd     The end date of the event.
     * @param allDay      Int that can be equal to 0 or 1.
     * @param duration    The duration of the event in RFC2445 format.
     */
    public BaseCalendarEvent(long id, int color, String title, String description, String location, long dateStart, long dateEnd, int allDay, String duration) {
        this.mId = id;
        this.mColor = color;
        this.mAllDay = (allDay == 1) ? true : false;
        this.mDuration = duration;
        this.mTitle = title;
        this.mDescription = description;
        this.mLocation = location;

        this.mStartTime = Calendar.getInstance();
        this.mStartTime.setTimeInMillis(dateStart);
        this.mEndTime = Calendar.getInstance();
        this.mEndTime.setTimeInMillis(dateEnd);
    }
````
Here is a quick (and very simple) example providing a list of events, you can provide several layouts too:

```java
    private void mockList(List<CalendarEvent> eventList) {
        Calendar startTime1 = Calendar.getInstance();
        Calendar endTime1 = Calendar.getInstance();
        endTime1.add(Calendar.MONTH, 1);
        BaseCalendarEvent event1 = new BaseCalendarEvent("Thibault travels in Iceland", "A wonderful journey!", "Iceland",
                ContextCompat.getColor(this, R.color.orange_dark), startTime1, endTime1, true);
        eventList.add(event1);

        Calendar startTime2 = Calendar.getInstance();
        startTime2.add(Calendar.DAY_OF_YEAR, 1);
        Calendar endTime2 = Calendar.getInstance();
        endTime2.add(Calendar.DAY_OF_YEAR, 3);
        BaseCalendarEvent event2 = new BaseCalendarEvent("Visit to Dalvík", "A beautiful small town", "Dalvík",
                ContextCompat.getColor(this, R.color.yellow), startTime2, endTime2, true);
        eventList.add(event2);

        // Example on how to provide your own layout
        Calendar startTime3 = Calendar.getInstance();
        Calendar endTime3 = Calendar.getInstance();
        startTime3.set(Calendar.HOUR_OF_DAY, 14);
        startTime3.set(Calendar.MINUTE, 0);
        endTime3.set(Calendar.HOUR_OF_DAY, 15);
        endTime3.set(Calendar.MINUTE, 0);
        DrawableCalendarEvent event3 = new DrawableCalendarEvent("Visit of Harpa", "", "Dalvík",
                ContextCompat.getColor(this, R.color.blue_dark), startTime3, endTime3, false, R.drawable.common_ic_googleplayservices);
        eventList.add(event3);
    }
````  

#  Methods

| Method                                    |       Description                           |
|-------------------------------------------|---------------------------------------------|
| void addEvent(CalendarEvent event)        | adding an Event                             |
| void addEvents(List<CalendarEvent events  | adding multiple Events.                     |
| int deleteEvent(CalendarEvent event)      | deleting an Event. return >= 0 if success, -2 if event is empty event, -1 if event not found |
| void updateEvent(CalendarEvent event)     | update an Event.                            |
| List<CalendarEvent> getEvents()           | get all events from calendar                |
| List<CalendarEvent> getEvents(int day, int month, int year) | get all events for specific date. Remember that month is start from zero |
| List<CalendarEvent> getEvents(int month, int year) | get all events from specific month. Remember that month is start from zero |
| boolean scrollToDate(int day, int month, int year) | scroll to specific date. Return true if scroll can be done, false if not. |
| boolean scrollToEvent(CalendarEvent event) | scroll to specific event. Return true if scroll can be done, false if not. |

# XML Attributes

| Attribute                           |         Description                                                                          |
|-------------------------------------|----------------------------------------------------------------------------------------------|
| app:acv_emptyEventVisibility        | Set visibility of empty calendar event value can be: View.VISIBLE, View.GONE, View.Invisible |



# Roadmap (feel free to suggest any other improvement ideas)

. Parallax items like in Google Calendar  
. Easier way to provide your own list of events (i.e when receiving objects from a web API)

# Participating?
Make your pull requests on feature or bug fix branches.

# Special thanks to these contributors
[FHellmann](https://github.com/FHellmann)

License
-------
[Licensed under the Apache License, Version 2.0](LICENSE)

    Copyright 2018 Rilix Technology
    Copyright 2015 Thibault Guégan
