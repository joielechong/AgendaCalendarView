package com.rilixtech.agendacalendarview.models;

import androidx.annotation.ColorInt;
import java.util.Calendar;

public interface CalendarEvent<T> {

  T placeholder(boolean placeholder);

  boolean isPlaceholder();

  public String getLocation();

  public T location(String location);

  int getLocationTextColor();

  T locationTextColor(int locationTextColor);

  long getId();

  T id(long id);

  Calendar getStartTime();

  T startTime(Calendar startCalendarTime);

  Calendar getEndTime();

  T endTime(Calendar endCalendarTime);

  String getTitle();

  T title(String title);

  int getTitleTextColor();

  T titleTextColor(int titleTextColor);

  Calendar getInstanceDay();

  T instanceDay(Calendar instanceDay);

  IDayItem getDayReference();

  T dayReference(IDayItem dayReference);

  IWeekItem getWeekReference();

  T weekReference(IWeekItem weekReference);


  T calendarDayColor(@ColorInt int color);

  @ColorInt int getCalendarDayColor();

  T copy();

  T visibility(int visibilityType);

  int getVisibility();

  int getColor();

  T color(int color);
}
