package com.rilixtech.sample;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import com.rilixtech.agendacalendarview.AgendaCalendarView;
import com.rilixtech.agendacalendarview.models.BaseCalendarEvent;
import com.rilixtech.agendacalendarview.models.CalendarEvent;
import com.rilixtech.agendacalendarview.models.IDayItem;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements
    AgendaCalendarView.AgendaCalendarViewListener {

  private static final String LOG_TAG = MainActivity.class.getSimpleName();

  private Toolbar mToolbar;
  private AgendaCalendarView mAgendaCalendarView;
  private TextView mTvDate;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    mToolbar = findViewById(R.id.activity_toolbar);
    mAgendaCalendarView = findViewById(R.id.agenda_calendar_view);
    mTvDate = findViewById(R.id.main_date_tv);

    setSupportActionBar(mToolbar);
    getSupportActionBar().setTitle("Agenda");
    mToolbar.setTitle("Agenda");

    // minimum and maximum date of our calendar
    // 2 month behind, one year ahead, example: March 2015 <-> May 2015 <-> May 2016
    Calendar minDate = Calendar.getInstance();
    Calendar maxDate = Calendar.getInstance();

    minDate.add(Calendar.MONTH, 0);
    minDate.set(Calendar.DAY_OF_MONTH, 1);
    maxDate.add(Calendar.YEAR, 0);

    List<CalendarEvent> eventList = new ArrayList<>();
    //mockList(eventList);

    List<Integer> weekends = new ArrayList<>();
    weekends.add(Calendar.SUNDAY);
    mAgendaCalendarView.setMinimumDate(minDate)
        .setMaximumDate(maxDate)
        .setCalendarEvents(eventList)
        //.setLocale(Locale.ENGLISH)
        //.setEventRender(new DrawableEventRenderer())
        .setAgendaCalendarViewListener(this)
        //.setWeekendsColor(getResources().getColor(android.R.color.background_dark))
        .setWeekends(weekends)
        //.setFirstDayOfWeek(Calendar.MONDAY)
        .build();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_add:
        Toast.makeText(this, "Add clicked", Toast.LENGTH_SHORT).show();
        addNewEvent();
        return true;
      case R.id.action_add_all:
        List<CalendarEvent> events = new ArrayList<>();
        mockList(events);
        mAgendaCalendarView.addEvents(events);
        return true;
      case R.id.action_scroll:
        // scroll to 2 days after now
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        day += 2;
        mAgendaCalendarView.scrollToDate(day, month, year);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void showPopUpMenu(View view, final CalendarEvent event) {
    PopupMenu popupMenu = new PopupMenu(this, view);
    popupMenu.inflate(R.menu.menu_calendar);
    popupMenu.setGravity(Gravity.END);

    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
      @Override public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.action_delete) {
          int position = mAgendaCalendarView.deleteEvent(event);
          Toast.makeText(MainActivity.this, "position = " + position, Toast.LENGTH_SHORT).show();
        } else if(item.getItemId() == R.id.action_update) {
          // change title of event
          event.title("New title now");
          mAgendaCalendarView.updateEvent(event);

        }
        return true;
      }
    });
    popupMenu.show();
  }

  private void addNewEvent() {
    Calendar startTime1 = Calendar.getInstance();
    Calendar endTime1 = Calendar.getInstance();
    int dayOfMonth = startTime1.get(Calendar.DAY_OF_MONTH) - 1;
    startTime1.set(Calendar.DAY_OF_MONTH, dayOfMonth);
    endTime1.set(Calendar.DAY_OF_MONTH, dayOfMonth);

    BaseCalendarEvent event4 = BaseCalendarEvent.prepareWith()
        .title("NEW ITEM")
        .description("NEW ITEM")
        .location("NEW ITEM")
        .color(ContextCompat.getColor(this, R.color.theme_event_confirmed))
        .startTime(startTime1)
        .endTime(endTime1)
        .allDay(false);
    mAgendaCalendarView.addEvent(event4);
  }

  @Override public void onDaySelected(IDayItem dayItem) {
    Log.d(LOG_TAG, String.format("Selected day: %s", dayItem));
    Toast.makeText(this, "dayItem = " + dayItem, Toast.LENGTH_SHORT).show();
  }

  @Override public void onEventClicked(View view, CalendarEvent event) {
    Log.d(LOG_TAG, String.format("Selected event: %s", event));
  }

  @Override public void onScrollToDate(Calendar calendar) {
    //if (getSupportActionBar() != null) {
    mTvDate.setText(
        calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
            + " "
            + calendar.get(Calendar.YEAR));
    //}
  }

  @Override public void onEventLongClicked(View view, CalendarEvent event) {
    showPopUpMenu(view, event);
  }

  private void mockList(List<CalendarEvent> eventList) {
    //Calendar startTime1 = Calendar.getInstance();
    //Calendar endTime1 = Calendar.getInstance();
    //endTime1.add(Calendar.MONTH, 1);
    //BaseCalendarEvent event1 = BaseCalendarEvent.prepareWith().title("Revisión requerimientos")
    //    .description("Diseño App")
    //    .location("Despacho 1")
    //    .id(0)
    //    .color(ContextCompat.getColor(this, R.color.theme_event_pending))
    //    .startTime(startTime1)
    //    .endTime(endTime1)
    //    //.drawableId(R.drawable.ic_launcher)
    //    .allDay(true);
    //
    //eventList.add(event1);

    //Calendar startTime2 = Calendar.getInstance();
    //startTime2.add(Calendar.DAY_OF_YEAR, 1);
    //Calendar endTime2 = Calendar.getInstance();
    //endTime2.add(Calendar.DAY_OF_YEAR, 3);

    Calendar startTime5 = Calendar.getInstance();
    Calendar endTime5 = Calendar.getInstance();
    startTime5.set(Calendar.HOUR_OF_DAY, 18);
    startTime5.set(Calendar.MINUTE, 0);
    endTime5.set(Calendar.HOUR_OF_DAY, 19);
    endTime5.set(Calendar.MINUTE, 0);

    DrawableCalendarEvent event5 = DrawableCalendarEvent.prepareWith()
        .drawableId(R.drawable.ic_launcher);
    event5.title("16:00 - 17:00 Reunión de coordinación AGN 3")
        .description("i")
        .location("Despacho 3")
        .id(1)
        .color(ContextCompat.getColor(this, android.R.color.holo_red_dark))
        .startTime(startTime5)
        .endTime(endTime5)
        .calendarDayColor(ContextCompat.getColor(this, android.R.color.holo_green_dark))
        .titleTextColor(ContextCompat.getColor(this, android.R.color.holo_blue_bright))
        .locationTextColor(ContextCompat.getColor(this, android.R.color.holo_purple))
        .allDay(false);

    eventList.add(event5);

    Calendar startTime6 = Calendar.getInstance();
    startTime6.add(Calendar.DAY_OF_YEAR, 1);
    Calendar endTime6 = Calendar.getInstance();
    endTime6.add(Calendar.DAY_OF_YEAR, 3);

    DrawableCalendarEvent event6 = DrawableCalendarEvent.prepareWith()
        .drawableId(R.drawable.ic_launcher);
    event6.title("Blacky number 1")
        .description("Our beloved song")
        .id(2)
        .location("Sala 2B")
        .color(ContextCompat.getColor(this, R.color.theme_event_pending))
        .startTime(startTime6)
        .endTime(endTime6)
        .allDay(true);
    eventList.add(event6);
  }
}
